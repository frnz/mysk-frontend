package frnz.tool.music.adapters.music.player;

import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class Mp3MetaAnalyzer implements SongMetaExtractor {

    private final Mp3File mp3file;

    public Mp3MetaAnalyzer(String fileName) {
        try {
            this.mp3file = new Mp3File(fileName);
        } catch (IOException | UnsupportedTagException | InvalidDataException e) {
            throw new RuntimeException("Could not get valid filename from " + fileName, e);
        }
    }

    @Override
    public String getFilename() {
        return mp3file.getFilename();
    }

    @Override
    public String getArtist() {
        return mp3file.hasId3v1Tag()
                ? mp3file.getId3v1Tag().getArtist()
                : mp3file.hasId3v2Tag() ? mp3file.getId3v2Tag().getArtist() : "";
    }

    @Override
    public String getTitle() {
        return mp3file.hasId3v1Tag()
                ? mp3file.getId3v1Tag().getTitle()
                : mp3file.hasId3v2Tag() ? mp3file.getId3v2Tag().getTitle() : "";
    }

    @Override
    public long getLength() {
        return mp3file.getLengthInSeconds();
    }

  // @Deprecated
  // private void readMetaData(String fileName) {
   //     log.warn("engine.mp3");
   //     mp3file = null;
   //     try {
   //         mp3file = new Mp3File(fileName);
   //         log.warn("Length of this mp3 is: " + mp3file.getLengthInSeconds() + " seconds");
   //         log.warn("Bitrate: " + mp3file.getBitrate() + " kbps " + (mp3file.isVbr() ? "(VBR)" : "(CBR)"));
   //         log.warn("Sample rate: " + mp3file.getSampleRate() + " Hz");
   //         log.warn("Has ID3v1 tag?: " + (mp3file.hasId3v1Tag() ? "YES" : "NO"));
   //         log.warn("Has ID3v2 tag?: " + (mp3file.hasId3v2Tag() ? "YES" : "NO"));
   //         log.warn("Has custom tag?: " + (mp3file.hasCustomTag() ? "YES" : "NO"));
   //         if (mp3file.hasId3v1Tag()) {
   //             ID3v1 id3v1Tag = mp3file.getId3v1Tag();
   //             log.warn("Track: " + id3v1Tag.getTrack());
   //             log.warn("Artist: " + id3v1Tag.getArtist());
   //             log.warn("Title: " + id3v1Tag.getTitle());
   //             log.warn("Album: " + id3v1Tag.getAlbum());
   //             log.warn("Year: " + id3v1Tag.getYear());
   //             log.warn("Genre: " + id3v1Tag.getGenre() + " (" + id3v1Tag.getGenreDescription() + ")");
   //             log.warn("Comment: " + id3v1Tag.getComment());
   //         }
   //         if (mp3file.hasId3v2Tag()) {
   //             ID3v2 id3v2Tag = mp3file.getId3v2Tag();
   //             log.warn("Track: " + id3v2Tag.getTrack());
   //             log.warn("Artist: " + id3v2Tag.getArtist());
   //             log.warn("Title: " + id3v2Tag.getTitle());
   //             log.warn("Album: " + id3v2Tag.getAlbum());
   //             log.warn("Year: " + id3v2Tag.getYear());
   //             log.warn("Genre: " + id3v2Tag.getGenre() + " (" + id3v2Tag.getGenreDescription() + ")");
   //             log.warn("Comment: " + id3v2Tag.getComment());
   //             log.warn("Lyrics: " + id3v2Tag.getLyrics());
   //             log.warn("Composer: " + id3v2Tag.getComposer());
   //             log.warn("Publisher: " + id3v2Tag.getPublisher());
   //             log.warn("Original artist: " + id3v2Tag.getOriginalArtist());
   //             log.warn("Album artist: " + id3v2Tag.getAlbumArtist());
   //             log.warn("Copyright: " + id3v2Tag.getCopyright());
   //             log.warn("URL: " + id3v2Tag.getUrl());
   //             log.warn("Encoder: " + id3v2Tag.getEncoder());
   //             byte[] albumImageData = id3v2Tag.getAlbumImage();
   //             if (albumImageData != null) {
   //                 log.warn("Have album image data, length: " + albumImageData.length + " bytes");
   //                 log.warn("Album image mime type: " + id3v2Tag.getAlbumImageMimeType());
   //             }
   //         }
   //         log.warn("Does the file have a id3v2Tag: " + (mp3file.hasId3v2Tag() ? "YES" : "NO"));
   //         if (mp3file.hasId3v2Tag()) {
   //             ID3v2 id3v2Tag = mp3file.getId3v2Tag();
   //             byte[] imageData = id3v2Tag.getAlbumImage();
   //             log.warn("Does the file have an album image: " + (imageData == null ? "NO" : "YES"));
   //             if (imageData != null) {
   //                 String mimeType = id3v2Tag.getAlbumImageMimeType();
   //                 // Write image to file - can determine appropriate file extension from the mime type
   //                 RandomAccessFile file = new RandomAccessFile("album-artwork", "rw");
   //                 file.write(imageData);
   //                 file.close();
   //             }
   //         }
   //     } catch (Exception e) {
   //         e.printStackTrace();
   //     }
   // }
}
