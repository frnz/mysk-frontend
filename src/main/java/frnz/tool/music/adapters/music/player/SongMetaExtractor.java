package frnz.tool.music.adapters.music.player;

public interface SongMetaExtractor {
    String getFilename();

    String getArtist();

    String getTitle();

    long getLength();
}
