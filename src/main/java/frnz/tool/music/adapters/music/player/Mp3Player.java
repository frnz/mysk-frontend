package frnz.tool.music.adapters.music.player;

import frnz.tool.music.core.boundaries.MusicPlayer;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;

public class Mp3Player implements MusicPlayer {
    private MediaPlayer currentSong;
    private Integer loadedSongId;

    @Override
    public void loadSong(String fileName, int id) {
        currentSong = new MediaPlayer(new Media(new File(fileName).toURI().toString()));
        loadedSongId = id;
    }

    @Override
    public Integer getIfOfLoadedSong() {
        return loadedSongId;
    }

    @Override
    public boolean isSongLoaded() {
        return currentSong != null;
    }

    /**
     *
     * @return true if song was
     */
    @Override
    public MediaPlayer.Status playOrPause() {
        if(currentSong == null) {
            return MediaPlayer.Status.UNKNOWN;
        }

        if (currentSong.getStatus().equals(MediaPlayer.Status.PLAYING)) {
            currentSong.pause();
            return MediaPlayer.Status.PLAYING;
        } else {
            currentSong.play();
            return MediaPlayer.Status.PAUSED;
        }
    }

    @Override
    public double getProgress() {
        return currentSong != null
                ? currentSong.getCurrentTime().toSeconds()
                : 0;
    }

    @Override
    public void mute() {
        if (currentSong != null) currentSong.setMute(!currentSong.isMute());
    }

    @Override
    public void setVolume(double newVolume) {
        if (currentSong != null) currentSong.setVolume(newVolume);
    }

    @Override
    public void seek(Duration to) {
        currentSong.seek(to);
    }

    @Override
    public void stop() {
        if (currentSong != null) currentSong.stop();
    }

    @Override
    public void skip_back() {
        if (currentSong != null) currentSong.seek(Duration.ZERO);
    }

    @Override
    public void skip_ahead() {
        if (currentSong != null) currentSong.seek(currentSong.getTotalDuration());
    }

    @Override
    public void setOnEndOfMedia(Runnable action) {
        currentSong.setOnEndOfMedia(action);
    }

    @Override
    public Duration getTotalDuration() {
        return currentSong.getTotalDuration();
    }

}

