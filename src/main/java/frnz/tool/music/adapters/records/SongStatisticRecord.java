package frnz.tool.music.adapters.records;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class SongStatisticRecord {
    private String id;
    private String songId;
    private long amountPlayed;
}
