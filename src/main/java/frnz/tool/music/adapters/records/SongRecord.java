package frnz.tool.music.adapters.records;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class SongRecord {
    private String _id;
    private String name;
    private String originalFileName;
    private Set<String> tags;
    private String insertDate;
    private String fullPath;
    private String creationDate;
}
