package frnz.tool.music.adapters.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FxApplication extends Application {

    public static boolean ENABLE_BORPA = true;
    private Scene scene;
    private boolean isNightmode = true;

    public static void main(String[] args) {
        FxApplication.launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("MYSK");
        primaryStage.initStyle(StageStyle.UNDECORATED);


        //try: https://stackoverflow.com/questions/42583779/how-to-change-the-color-of-title-bar-in-framework-javafx
        primaryStage.getIcons().add(new Image(FxApplication.class.getClassLoader().getResourceAsStream("icon.gif")));

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FxApplication.class.getClassLoader().getResource("MainUserInterface.fxml"));
        var topBar = topBar();
        Parent rootLayout = loader.load();
        topBar.setBottom(rootLayout);
        scene = new Scene(topBar, 1220, 800);
        primaryStage.setScene(scene);
        primaryStage.setResizable(true);
        primaryStage.show();
        log.info("Showing primary stage!");
        scene.getStylesheets().add(getClass().getResource("/mainStyle.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource("/colorsNight.css").toExternalForm());
        ResizeHelper.addResizeListener(primaryStage);

        // allow the clock background to be used to drag the clock around.
        final Delta dragDelta = new Delta();
        topBar.getTop().setOnMousePressed(mouseEvent -> {
            // record a delta distance for the drag and drop operation.
            dragDelta.x = primaryStage.getX() - mouseEvent.getScreenX();
            dragDelta.y = primaryStage.getY() - mouseEvent.getScreenY();
        });
        topBar.getTop().setOnMouseDragged(mouseEvent -> {
            primaryStage.setX(mouseEvent.getScreenX() + dragDelta.x);
            primaryStage.setY(mouseEvent.getScreenY() + dragDelta.y);
        });

    }

    private BorderPane topBar() {
        var outer = new BorderPane();
        var inner = new BorderPane();
        var toolBar = new ToolBar();
        var windowButtons = new WindowButtons();

        int height = 25;
        toolBar.setPrefHeight(height);
        toolBar.setMinHeight(height);
        toolBar.setMaxHeight(height);
        toolBar.getItems().add(windowButtons);
        toolBar.setId("inner_toolbar");
        inner.setId("inner_header");
        inner.setRight(toolBar);
        outer.setTop(inner);

        return outer;
    }

    class WindowButtons extends HBox {

        public WindowButtons() {
            var closeBtn = new Button("8xt");
            closeBtn.setOnAction(actionEvent -> Platform.exit());
            closeBtn.setId("btn_exit");

            var borpa = new Button("ns");
            borpa.setOnAction(actionEvent -> FxApplication.ENABLE_BORPA = !FxApplication.ENABLE_BORPA);
            borpa.setId("btn_noborpa");

            var night = new Button("N8");
            night.setOnAction(actionEvent -> toggleNightmode());
            night.setId("btn_night");

            var separator1 = new Separator();
            separator1.setId("windows_separator");
            var separator2 = new Separator();
            separator2.setId("windows_separator");
            this.getChildren().addAll(borpa, separator1, night, separator2, closeBtn);
        }
    }


    public void toggleNightmode() {
        if (!isNightmode) {
            log.info("Enabling Nightmode");
            scene.getStylesheets().clear();
            scene.getStylesheets().add(getClass().getResource("/mainStyle.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("/colorsNight.css").toExternalForm());
            isNightmode = true;
        } else {
            log.info("Enabling Daymode");
            scene.getStylesheets().clear();
            scene.getStylesheets().add(getClass().getResource("/mainStyle.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("/colorsDay.css").toExternalForm());
            isNightmode = false;
        }
    }


    static class Delta {
        double x, y;
    }
}
