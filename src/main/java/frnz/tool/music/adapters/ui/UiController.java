package frnz.tool.music.adapters.ui;

import frnz.tool.music.adapters.caches.ToBeImportedCache;
import frnz.tool.music.adapters.caches.ToBeImportedCache.ToBeImportedSong;
import frnz.tool.music.adapters.music.player.Mp3MetaAnalyzer;
import frnz.tool.music.adapters.music.player.Mp3Player;
import frnz.tool.music.adapters.mysk.MyskStatistics;
import frnz.tool.music.adapters.mysk.RestSongRepository;
import frnz.tool.music.adapters.ui.components.TagBar;
import frnz.tool.music.core.boundaries.MusicPlayer;
import frnz.tool.music.core.boundaries.SongRepository;
import frnz.tool.music.core.boundaries.StatisticsRepository;
import frnz.tool.music.core.enities.Song;
import frnz.tool.music.core.interactors.AddNewSong;
import frnz.tool.music.core.interactors.GetAllSongs;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static frnz.tool.music.adapters.ui.FxApplication.ENABLE_BORPA;

@Slf4j
public class UiController implements Initializable {

    private final DirectoryChooser directoryChooser = new DirectoryChooser();
    private final ToBeImportedCache toBeImportedCache = new ToBeImportedCache();

    @FXML
    private AnchorPane ap;

    @FXML
    private TextArea dbDebug;

    @FXML
    private VBox playmusicbox;

    @FXML
    private TextField search_text;

    @FXML
    private VBox play_right_container;

    @FXML
    private TextField bulk_folder_text;

    @FXML
    private Button bulk_folder_button;

    @FXML
    private VBox bulk_import_music_box;

    @FXML
    private ImageView borpa;

    ////////////////////////////////////// player

    @FXML
    private Button player_play;
    @FXML
    private Button player_skip_ahead;
    @FXML
    private Button player_skip_back;
    @FXML
    private Button player_stop;
    @FXML
    private Button player_mute;
    @FXML
    private Slider player_volume;
    @FXML
    private ProgressBar player_progress;
    @FXML
    private Label current_song;

    @FXML
    private AnchorPane player_pane;

    ////////////////////////////////////// player


    @FXML
    private Label quickstats_label_songs_count;

    private File selectedFolder;
    private MusicPlayer musicPlayer = new Mp3Player();

    private SongRepository songRepository = new RestSongRepository();
    private StatisticsRepository statistics = new MyskStatistics();
    private int failedRandomSongCounter = 0;
    private List<Song> currentSongSelection;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        var startTime = System.currentTimeMillis();
        log.info("Init controller");
        GetAllSongs getAllSongs = new GetAllSongs(songRepository);
        getAllSongs.execute();
        List<Song> result = getAllSongs.getResult();
        updateSongCountLabel(result.size());
        log.info("Initially loading songs took {}ms", System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        fillSongList(result);
        log.info("Initially filling the list of songs in the ui took {}ms", System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        //player_pane.setEffect(new GaussianBlur()); blur the player

        player_volume.valueProperty().addListener((obs, oldValue, newValue) -> {
            log.trace("Updating volume from {} to {}", oldValue.doubleValue() / 100, newValue.doubleValue() / 100);
            musicPlayer.setVolume(newValue.doubleValue() / 100);
        });

        var progressbarUpdater = new Timeline(new KeyFrame(javafx.util.Duration.millis(100),
                event -> updateProgress()));
        progressbarUpdater.setCycleCount(Timeline.INDEFINITE);
        progressbarUpdater.play();

        //https://stackoverflow.com/questions/45039001/click-on-progressbar-to-seek-mediaplayer-javafx-swing
        player_progress.setCursor(Cursor.CROSSHAIR);
        player_progress.setOnMouseClicked(
                event -> {
                    int point = (int) Math.round((event.getX() / player_progress.getWidth()) * musicPlayer.getTotalDuration().toMillis());
                    var pointDuration = javafx.util.Duration.millis(point);
                    log.trace("Trying to skip to a certain point. Progress={} duration={} mouseX={} width={} max={}",
                            musicPlayer.getProgress(), pointDuration, event.getX(), player_progress.getWidth(), musicPlayer.getTotalDuration());
                    musicPlayer.seek(pointDuration);
                }
        );
        log.info("Initiating the rest (progress+listeners) took {}ms", System.currentTimeMillis() - startTime);
    }

    private String stripUnwanted(String text) {
        if (text == null || text.isBlank()) {
            return "";
        }

        String[] removeExpressions = {"HD", "hd", "[", "]", "album",
                "full", "]", "soundtrack", "mix", "ost",
                "original", "AMV", "high quality", "official", "VEVO",
                "Official Music Video", "Official", "Video", "FAN", "SONG"
        };
        for (String expression : removeExpressions) {
            text = text.replace(expression, "");
        }
        return text;
    }

    private String guessTags(String name) {
        return containsHanScript(name) ? "asian" : "";
    }

    public static boolean containsHanScript(String s) {
        return s.codePoints().anyMatch(
                codepoint ->
                        Character.UnicodeScript.of(codepoint) == Character.UnicodeScript.HAN);
    }

    private String guessName(String name) {
        return name
                //      .split("-")[0]
                .split("\\[")[0]
                .split("_")[0];
    }

    public void playPause() {
        if (!musicPlayer.isSongLoaded()) {
            playRandomSong();
        } else {
            var status = musicPlayer.playOrPause();
            if (status.equals(MediaPlayer.Status.PLAYING)) {
                statistics.pauseRecording();
            } else {
                statistics.unpauseRecording();
            }
        }

        if (player_play.getText().equals(">")) {
            player_play.setText("||");
            if (ENABLE_BORPA) borpa.setImage(new Image("borpa.gif"));
        } else {
            player_play.setText(">");
            borpa.setImage(new Image("borpaResize.png"));
        }
    }

    public void mute() {
        musicPlayer.mute();
        if (player_mute.getText().equals("mute")) {
            player_mute.setText("unmute");
        } else {
            player_mute.setText("mute");
        }
    }

    public void stop() {
        musicPlayer.stop();
    }

    public void skip_ahead() {
        musicPlayer.skip_ahead();
    }

    public void skip_back() {
        musicPlayer.skip_back();
    }

    public void searchSong() {
        var searchText = search_text.getText();

        if (searchText.length() == 0) {
            fillSongList(songRepository.getAll());
            return;
        }

        // dont search for 1 or 2 characters
        if (searchText.length() <= 2) {
            return;
        }

        if (searchText.isBlank()) {
            fillSongList(songRepository.getAll());
            return;
        }

        List<Song> result = new ArrayList<>();

        //search for song-names
        result.addAll(songRepository.findByNameContains(searchText));

        //only if nothing found: search for artist
        if (result.isEmpty()) {
            result.addAll(songRepository.findByArtist(searchText));
        }

        List<String> filteredSongs = Arrays.stream(searchText.split(",")).collect(Collectors.toList());
        result.addAll(filteredSongs.isEmpty()
                ? songRepository.getAll()
                : songRepository.findByTags(filteredSongs));

        fillSongList(result);
    }

    public void selectFolder() {
        Stage stage = (Stage) ap.getScene().getWindow();
        File folder = directoryChooser.showDialog(stage);
        if (folder == null) {
            log.info("No folder selected.");
            return;
        }
        bulk_folder_text.setText(folder.getAbsolutePath());
        selectedFolder = folder;
        fillBulkImport();
    }

    public void fillBulkImport() {
        try (Stream<Path> walk = Files.walk(Paths.get(selectedFolder.toURI()))) {
            toBeImportedCache.clear();
            bulk_import_music_box.getChildren().clear();
            walk.filter(Files::isRegularFile)
                    .filter(s -> s.toString().endsWith(".mp3"))
                    .forEach(name -> bulk_import_music_box.getChildren().add(createBulkImportNode(name.toFile())));
        } catch (IOException e) {
            log.warn("Could not read files in folder.", e);
        }
    }

    private void updateProgress() {
        if (musicPlayer.getProgress() > 0) {
            double progress = musicPlayer.getProgress() / musicPlayer.getTotalDuration().toSeconds();
            log.trace("Progress is {} / {} = {}", musicPlayer.getProgress(), musicPlayer.getTotalDuration(), progress);
            player_progress.setProgress(progress);
        }
    }

    private void fillSongList(List<Song> songs) {
        var startTime = System.currentTimeMillis();

        updateSongCountLabel(songs.size());
        Collections.shuffle(songs);

        playmusicbox.getChildren().clear();
        playmusicbox.setSpacing(5);
        playmusicbox.setPadding(new Insets(5));

        currentSongSelection = songs;  // for weighted random next song

        var songNodes = songs.stream().map(this::createSongNode).collect(Collectors.toList());

        log.debug("Fill song-list perpetration took {}ms", System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        songNodes.forEach(node -> playmusicbox.getChildren().add(node));
        log.debug("Fill song-list execution took {}ms", System.currentTimeMillis() - startTime);
    }

    private Node createBulkImportNode(File file) {
        UUID songId = toBeImportedCache.add(ToBeImportedSong.builder()
                .file(file)
                .build());
        var row = new BorderPane();
        row.setMinWidth(1160);
        row.setMaxWidth(1160);
        row.setLeft(createImportDescription(file, songId));
        row.setRight(createImportButton(songId));
        row.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        row.setId(songId.toString());

        if (songRepository.existsByOriginalFileName(file.getName())) {
            row.setLeft(new Label(file.getName()));
            row.setRight(new Label("Already Imported"));
            row.setId("import_node_row");
        }

        return row;
    }

    private Node createSongNode(Song song) {
        var songRow = new BorderPane();
        songRow.setMinWidth(775);
        songRow.setMaxWidth(775);
        songRow.setLeft(createDescription(song));
        songRow.setRight(createPlayButton(song));
        songRow.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        //     var stops = new Stop[]{new Stop(0.3, Color.WHITE), new Stop(1, Color.GREY)};
        //     var lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        //     var fills = new BackgroundFill(lg1, null, null);
        //     songRow.setBackground(new Background(fills));
        songRow.setId(song.getOriginalFileName());

        return songRow;
    }

    private Node createDescription(Song song) {
        // beware! this is very time/resource heavy as we have to open lots of files!
//        SongMetaExtractor extractor = new Mp3MetaAnalyzer(song.getFullPath());
//        var lengthText = String.format("[%02d:%02d:%02d]", //(art=%s|tit=%s)
//                Duration.ofSeconds(extractor.getLength()).toHoursPart(),
//                Duration.ofSeconds(extractor.getLength()).toMinutesPart(),
//                Duration.ofSeconds(extractor.getLength()).toSecondsPart());

        var songName = new Label(" " + song.getName());
        songName.setId("song_node_label_songname");

        var artist = new Label(" (" + song.getArtist() + ")");
        artist.setId("song_node_label_artist");
        if (song.getArtist() == null || song.getArtist().isBlank()) {
            artist.setText("");
        }

        var rating = new Label(" " + song.getRating());
        rating.setId("rating-label");
        updateSongRatingColor(song, rating);

        rating.setOnMouseClicked(me -> {
            int newRating = song.getRating();
            if (me.getButton().equals(MouseButton.PRIMARY)) {
                newRating++;
            } else if (me.getButton().equals(MouseButton.SECONDARY)) {
                newRating--;
            }
            this.songRepository.updateRating(song.getId(), newRating);
            song.setRating(newRating);
            rating.setText(" " + newRating);
            updateSongRatingColor(song, rating);
        });

        var separator = new Label("|");
        separator.setId("rating-separator");

        var textContainer = new HBox();
        textContainer.getChildren().addAll(/*length,*/ rating, separator, songName, artist);
        textContainer.setMinWidth(320);
        textContainer.setMaxWidth(320);
        // Tooltip.install(textContainer, new Tooltip(lengthText + "  " + song.getFullPath()));

        var songTags = new TagBar();
        songTags.getTags().addAll(song.getTags());
        HBox leftContainer = new HBox();
        leftContainer.getChildren().addAll(textContainer, songTags);

        return leftContainer;
    }

    private void updateSongRatingColor(Song song, Label rating) {
        if (song.getRating() < 20) {
            rating.setStyle("-fx-text-fill: #a5a397"); //grey (original -> #666659)
        } else if (song.getRating() < 50) {
            rating.setStyle("-fx-text-fill: #a5ff77"); //green (original -> #1eff00)
        } else if (song.getRating() < 70) {
            rating.setStyle("-fx-text-fill: #68afff"); //blue (original -> #0070ff)
        } else if (song.getRating() < 90) {
            rating.setStyle("-fx-text-fill: #b365f8"); //purple (original -> #a330c9)
        } else if (song.getRating() >= 90) {
            rating.setStyle("-fx-text-fill: #ffaf3a"); //orange (original -> #ff8000)
        }
    }

    private Button createPlayButton(Song song) {
        var play = new Button("play");
        play.setMinWidth(38);
        play.setOnAction((a) -> {
            Optional<Node> currentSong = playmusicbox.getChildren().stream()
                    .filter(c -> c.getId().equals(song.getOriginalFileName()))
                    .findAny();
            playmusicbox.getChildren().forEach(n -> n.setEffect(null));
            currentSong.ifPresent(c -> c.setEffect(new ColorAdjust(-0.5, 0, 0, 0)));
            musicPlayer.stop();

            // update time for previously playing song
            if (musicPlayer.isSongLoaded()) {
                var secondsPlayed = statistics.stopRecording();
                statistics.updateListenTime(musicPlayer.getIfOfLoadedSong(), secondsPlayed);
            }

            musicPlayer.loadSong(song.getFullPath(), song.getId());
            musicPlayer.setVolume(player_volume.getValue() / 100);
            musicPlayer.playOrPause();
            statistics.startRecording();
            onEndOfMedia();
            player_play.setText("||");
            if (ENABLE_BORPA) borpa.setImage(new Image("borpa.gif"));
            player_pane.setEffect(null);
            current_song.setText(" " + song.getName());
        });
        return play;
    }

    private void onEndOfMedia() {
        musicPlayer.setOnEndOfMedia(() -> {
            var secondsPlayed = statistics.stopRecording();
            statistics.updateListenTime(musicPlayer.getIfOfLoadedSong(), secondsPlayed);
            borpa.setImage(new Image("borpaResize.png"));
            playRandomSong();
        });
    }

    private void playRandomSong() {
//        ObservableList<Node> children = playmusicbox.getChildren();
//        int rnd = (int) (Math.random() * children.size());
//        String songId = children.get(rnd).getId();

        double totalWeight = 0;
        for (Song song : currentSongSelection) {
            totalWeight += song.getRating();
        }

        int idx = 0;
        for (double r = Math.random() * totalWeight; idx < currentSongSelection.size() - 1; ++idx) {
            r -= currentSongSelection.get(idx).getRating();
            if (r <= 0.0) break;
        }
        String songId = String.valueOf(currentSongSelection.get(idx).getOriginalFileName());

        var songs = songRepository.findByOriginalFileName(songId);
        var song = songs.get(0);
        if (song.getRating() <= 40 && failedRandomSongCounter <= 10) {
            failedRandomSongCounter++;
            log.info("Rating of random song {} to low, skipping ({}/10)!", songId, failedRandomSongCounter);
            playRandomSong();
            return;
        }
        failedRandomSongCounter = 0;
        musicPlayer.loadSong(song.getFullPath(), song.getId());
        musicPlayer.setVolume(player_volume.getValue() / 100);
        musicPlayer.playOrPause();
        statistics.startRecording();

        if (ENABLE_BORPA) borpa.setImage(new Image("borpa.gif"));
        current_song.setText(" " + song.getName());
        Optional<Node> currentSong = playmusicbox.getChildren().stream()
                .filter(c -> c.getId().equals(songId))
                .findAny();
        playmusicbox.getChildren().forEach(n -> n.setEffect(null));
        currentSong.ifPresent(c -> c.setEffect(new ColorAdjust(-0.5, 0, 0, 0)));
        onEndOfMedia();
    }

    private Node createImportDescription(File file, UUID songId) {
        var leftText = new Label(file.getName().replace(".mp3", ""));
        leftText.setMinWidth(370);
        leftText.setMaxWidth(370);
        var songTags = new TagBar();
        var guessedTags = guessTags(file.getName()).toLowerCase();
        var extractor = new Mp3MetaAnalyzer(file.getAbsolutePath());
        if (!guessedTags.isBlank()) songTags.getTags().add(guessedTags);
        var extractedArtist = extractor.getArtist();
        //      if (extractedArtist != null && !extractedArtist.isBlank()) songTags.getTags().add(extractedArtist);
        var leftContainer = new HBox();

        var newSongName = new TextField();
        newSongName.setMinWidth(240);
        newSongName.setFocusTraversable(false);
        newSongName.setPromptText("song name");
        newSongName.setText(stripUnwanted(guessName(file.getName())).replace(".mp3", ""));

        var artist = new TextField();
        artist.setMinWidth(140);
        artist.setFocusTraversable(false);
        artist.setPromptText("artist");
        artist.setText(stripUnwanted(extractedArtist).toLowerCase());

        leftContainer.getChildren().addAll(leftText, newSongName, artist, songTags);
        newSongName.setOnAction(a -> updateToBeImportedSong(songId, songTags, newSongName, artist));
        artist.setOnAction(a -> updateToBeImportedSong(songId, songTags, newSongName, artist));
        songTags.addEventHandler(Event.ANY, a -> updateToBeImportedSong(songId, songTags, newSongName, artist));
        updateToBeImportedSong(songId, songTags, newSongName, artist);
        return leftContainer;
    }

    private void updateToBeImportedSong(UUID songId, TagBar songTags, TextField songName, TextField artist) {
        ToBeImportedSong toBeImportedSong = toBeImportedCache.get(songId);
        toBeImportedSong.setNewSongName(songName.getText());
        toBeImportedSong.setArtist(artist.getText());
        toBeImportedSong.setTags(new HashSet<>(songTags.getTags()));
    }

    private Button createImportButton(UUID songId) {
        Button importButton = new Button("import");
        importButton.setMaxWidth(60);
        importButton.setFocusTraversable(false);
        ToBeImportedSong toBeImportedSong = toBeImportedCache.get(songId);
        importButton.setOnAction((a) -> {
            if (toBeImportedSong != null) {
                String newPath = moveToManagedDir(toBeImportedSong);
                log.info("Importing file selected: {}", toBeImportedSong.getFile().getName());
                new AddNewSong(
                        new File(newPath),
                        songRepository,
                        toBeImportedSong.getNewSongName(),
                        toBeImportedSong.getArtist(),
                        toBeImportedSong.getTags()
                ).execute();
            }
            bulk_import_music_box.getChildren().stream()
                    .filter(n -> n.getId().equals(songId.toString()))
                    .forEach(node -> {
                        var test = (BorderPane) node;
                        test.setLeft(new Label(toBeImportedSong.getFile().getName()));
                        test.setRight(new Label("Already Imported"));
                        test.setId("import_node_row");
                    });
            //loadCurrentSongs();
        });
        return importButton;
    }

    private String moveToManagedDir(ToBeImportedSong song) {
        String newPath = String.format("D:\\musicv2%s%s", File.separator, song.getFile().getName());
        try {
            Files.copy(song.getFile().toPath(), Paths.get(
                    newPath),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Could not move file {}", song.getFile().getAbsolutePath(), e);
        }
        return newPath;
    }


    private void updateSongCountLabel(int size) {
        quickstats_label_songs_count.setText(size + "");
    }
}
