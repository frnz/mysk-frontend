package frnz.tool.music.adapters.caches;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.*;

public class ToBeImportedCache {

    @Builder
    @Getter
    @Setter
    public static class ToBeImportedSong {
        private final File file;
        private String newSongName;
        private String artist;
        private Set<String> tags;
    }

    private Map<UUID, ToBeImportedSong> songs = new HashMap<>();

    public UUID add(ToBeImportedSong song) {
        UUID uuid = UUID.randomUUID();
        songs.put(uuid, song);
        return uuid;
    }

    public ToBeImportedSong get(UUID id) {
        return songs.get(id);
    }

    public void clear() {
        songs.clear();
    }


}
