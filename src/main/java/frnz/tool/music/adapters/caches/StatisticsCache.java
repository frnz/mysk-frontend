package frnz.tool.music.adapters.caches;

import com.google.gson.Gson;
import frnz.tool.music.core.boundaries.StatisticsRepository;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static java.time.Instant.now;

@Slf4j
public class StatisticsCache implements StatisticsRepository {

    Map<Integer, Long> songIdToSecondsListened = new HashMap<>();
    private Instant startPlayed;
    private Instant startPaused;
    private long totalSecondsPaused = 0;

    @Override
    public void updateListenTime(Integer songId, Long secondsListened) {
        songIdToSecondsListened.computeIfPresent(songId, (k, v) -> v + secondsListened);
        songIdToSecondsListened.putIfAbsent(songId, secondsListened);
        log.info("Statistics" + new Gson().toJson(songIdToSecondsListened));
    }

    @Override
    public void updateSkipCount(Integer songId) {

    }

    @Override
    public void startRecording() {
        startPlayed = now();
    }

    @Override
    public long stopRecording() {
        if (startPaused != null) {
            unpauseRecording();
        }

        var duration = Duration.between(startPlayed, now()).toSeconds();
        var durationWithoutPauses = duration - totalSecondsPaused;
        log.info("Stopped recording for current song. seconds total = {}, pauses = {}, seconds listened = {}",
                duration, totalSecondsPaused, durationWithoutPauses
        );

        startPlayed = null;
        startPaused = null;
        totalSecondsPaused = 0;
        return durationWithoutPauses;
    }

    @Override
    public void pauseRecording() {
        startPaused = now();
    }

    @Override
    public void unpauseRecording() {
        totalSecondsPaused += Duration.between(startPaused, now()).toSeconds();
        startPaused = null;
    }

}
