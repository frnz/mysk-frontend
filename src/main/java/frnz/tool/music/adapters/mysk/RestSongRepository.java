package frnz.tool.music.adapters.mysk;

import com.google.gson.Gson;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import frnz.tool.music.core.boundaries.SongRepository;
import frnz.tool.music.core.enities.Song;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
public class RestSongRepository implements SongRepository {

    private final Gson gson = new Gson();

    @Override
    public void save(Song song) {
        log.info("Posting song to server. [{}]", song.getOriginalFileName());
        try {
            var songJson = gson.toJson(song);
            var result = Unirest.post("http://localhost:8888/songs")
                    .body(songJson).asBinary();
            log.info("Posted Song to Server [{}] [{}]", result.getStatusText(), song.getOriginalFileName());
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
        }
    }

    @Override
    public List<Song> getAll() {
        log.info("Getting all songs from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs").asString();
            var songsJson = gson.fromJson(result.getBody(), Song[].class);
            log.info("GET Songs returned {} songs.", songsJson.length);
            return Arrays.asList(songsJson);
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Song> findByTags(List<String> collect) {
        log.info("Getting songs by tags from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs")
                    .queryString("tags", String.join(",", collect))
                    .asString();
            var songsJson = gson.fromJson(result.getBody(), Song[].class);
            log.info("GET Songs returned {} songs.", songsJson.length);
            return Arrays.asList(songsJson);
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Song> findByOriginalFileName(String name) {
        log.info("Getting songs by originalFileName from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs")
                    .queryString("originalfilename", name)
                    .asString();
            var songsJson = gson.fromJson(result.getBody(), Song[].class);
            log.info("GET Songs returned {} songs.", songsJson.length);
            return Arrays.asList(songsJson);
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return Collections.emptyList();
        }
    }

    @Override
    public boolean existsByOriginalFileName(String name) {
        log.info("Finding out if song exists by originalFileName from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs")
                    .queryString("exists", name)
                    .asString();
            boolean doesSongExist = result.getBody() == null || result.getBody().equals("true");
            log.info("GET Song exists returned {}.", doesSongExist);
            return doesSongExist;
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return true;
        }
    }


    @Override
    public List<Song> findByNameContains(String query) {
        log.info("Getting songs by name contains from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs")
                    .queryString("namequery", query)
                    .asString();
            var songsJson = gson.fromJson(result.getBody(), Song[].class);
            log.info("GET Songs returned {} songs.", songsJson.length);
            return Arrays.asList(songsJson);
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return Collections.emptyList();
        }
    }

    @Override
    public List<Song> findByArtist(String artist) {
        log.info("Getting songs by artist from server.");
        try {
            var result = Unirest.get("http://localhost:8888/songs")
                    .queryString("artist", artist)
                    .asString();
            var songsJson = gson.fromJson(result.getBody(), Song[].class);
            log.info("GET Songs returned {} songs.", songsJson.length);
            return Arrays.asList(songsJson);
        } catch (UnirestException e) {
            log.warn("Error while creating song", e);
            return Collections.emptyList();
        }
    }

    @Override
    public void updateRating(Integer songId, Integer newRating) {
        log.info("Updating song rating in the server.");
        try {
            var result = Unirest.put("http://localhost:8888/songs/" + songId + "/rating/" + newRating)
                    .asString().getStatusText();
            log.info("PUT Song rating result = [{}]",result);
        } catch (UnirestException e) {
            log.warn("Error while updating song rating", e);
        }
    }

}
