package frnz.tool.music.adapters.mysk;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import frnz.tool.music.core.boundaries.StatisticsRepository;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;

import static java.time.Instant.now;

@Slf4j
public class MyskStatistics implements StatisticsRepository {

    private Instant startPlayed;
    private Instant startPaused;
    private long totalSecondsPaused = 0;

    @Override
    public void updateListenTime(Integer songId, Long secondsListened) {
        log.info("Updating time listened in the server.");
        try {
            var result = Unirest.put("http://localhost:8888/songs/" + songId + "/played/" + secondsListened)
                    .asString().getStatusText();
            log.info("PUT Song rating result = [{}]",result);
        } catch (UnirestException e) {
            log.warn("Error while updating song rating", e);
        }
    }

    @Override
    public void updateSkipCount(Integer songId) {

    }

    @Override
    public void startRecording() {
        startPlayed = now();
    }

    @Override
    public long stopRecording() {
        if (startPaused != null) {
            unpauseRecording();
        }

        var duration = Duration.between(startPlayed, now()).toSeconds();
        var durationWithoutPauses = duration - totalSecondsPaused;
        log.info("Stopped recording for current song. seconds total = {}, pauses = {}, seconds listened = {}",
                duration, totalSecondsPaused, durationWithoutPauses
        );

        startPlayed = null;
        startPaused = null;
        totalSecondsPaused = 0;
        return durationWithoutPauses;
    }

    @Override
    public void pauseRecording() {
        startPaused = now();
    }

    @Override
    public void unpauseRecording() {
        totalSecondsPaused += Duration.between(startPaused, now()).toSeconds();
        startPaused = null;
    }

}
