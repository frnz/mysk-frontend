package frnz.tool.music.adapters.mongodb;

import com.google.gson.Gson;
import com.mongodb.ConnectionString;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import frnz.tool.music.adapters.records.SongRecord;
import frnz.tool.music.core.boundaries.SongRepository;
import frnz.tool.music.core.enities.Song;
import lombok.RequiredArgsConstructor;
import org.bson.Document;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public class MongoSongRepository implements SongRepository {

    private final MongoClient mongoClient = MongoClients.create(new ConnectionString("mongodb://localhost"));
    private final Gson gson = new Gson();

    @Override
    public void save(Song song) {

        SongRecord record = songToRecord(song);

        MongoDatabase database = mongoClient.getDatabase("music");
        MongoCollection<Document> collection = database.getCollection("songs");
        collection.insertOne(Document.parse(gson.toJson(record)));
    }

    @Override
    public List<Song> getAll() {
        MongoDatabase database = mongoClient.getDatabase("music");
        MongoCollection<Document> collection = database.getCollection("songs");
        FindIterable<Document> documents = collection.find();
        List<Song> result = new ArrayList<>();
        for (Document document : documents) {
            result.add(gson.fromJson(document.toJson(), Song.class));
        }
        return result;
    }

    @Override
    public List<Song> findByTags(List<String> collect) {
        MongoDatabase database = mongoClient.getDatabase("music");
        MongoCollection<Document> collection = database.getCollection("songs");
        FindIterable<Document> documents = collection.find(Filters.in("tags",collect));
        List<Song> result = new ArrayList<>();
        for (Document document : documents) {
            result.add(gson.fromJson(document.toJson(), Song.class));
        }
        return result;
    }

    @Override
    public List<Song> findByOriginalFileName(String name) {
        MongoDatabase database = mongoClient.getDatabase("music");
        MongoCollection<Document> collection = database.getCollection("songs");
        FindIterable<Document> documents = collection.find(Filters.eq("originalFileName",name));
        List<Song> result = new ArrayList<>();
        for (Document document : documents) {
            result.add(gson.fromJson(document.toJson(), Song.class));
        }
        return result;
    }

    @Override
    public boolean existsByOriginalFileName(String name) {
        return false;
    }

    @Override
    public List<Song> findByNameContains(String ignore) {
        return Collections.emptyList();
    }

    @Override
    public List<Song> findByArtist(String ignore) {
        return Collections.emptyList();
    }

    @Override
    public void updateRating(Integer songId, Integer newRating) {
        // ignored
    }

    private SongRecord songToRecord(Song song) {
        return SongRecord.builder()
                ._id(UUID.nameUUIDFromBytes(song.getName().getBytes()).toString())
                .name(song.getName())
                .originalFileName(song.getOriginalFileName())
                .tags(song.getTags())
                .insertDate(Instant.now().toString())
                .creationDate(song.getCreationDate())
                .fullPath(song.getFullPath())
                .build();
    }
}
