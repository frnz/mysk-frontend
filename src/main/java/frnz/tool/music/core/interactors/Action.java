package frnz.tool.music.core.interactors;

public interface Action {
    void execute();
}
