package frnz.tool.music.core.interactors;

import frnz.tool.music.core.boundaries.SongRepository;
import frnz.tool.music.core.enities.Song;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class GetAllSongs implements Action{

    private final SongRepository repository;
    private List<Song> result;

    @Override
    public void execute() {
        result = repository.getAll();
    }

    public List<Song> getResult() {
        return result;
    }
}
