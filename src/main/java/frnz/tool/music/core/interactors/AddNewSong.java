package frnz.tool.music.core.interactors;

import frnz.tool.music.core.boundaries.SongRepository;
import frnz.tool.music.core.enities.Song;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
public class AddNewSong implements Action {

    private final File file;
    private final SongRepository repository;
    private final String newSongName;
    private final String artist;
    private final Set<String> tags;

    @Override
    public void execute() {
        try {
            BasicFileAttributes attributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
            FileTime creationTime = attributes.creationTime();
            Song song = Song.builder()
                    .name(newSongName)
                    .artist(artist)
                    .originalFileName(file.getName())
                    .fullPath(file.getAbsolutePath())
                    .creationDate(fileTimeToString(creationTime))
                    .tags(tags)
                    .build();
            repository.save(song);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String fileTimeToString(FileTime fileTime) {
        return Instant.ofEpochMilli(fileTime.toMillis()).toString();
    }


}
