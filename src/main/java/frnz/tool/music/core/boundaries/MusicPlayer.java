package frnz.tool.music.core.boundaries;

import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public interface MusicPlayer {
    void loadSong(String fileName, int id);
    Integer getIfOfLoadedSong();
    boolean isSongLoaded();
    MediaPlayer.Status playOrPause();
    double getProgress();
    void mute();
    void setVolume(double newVolume);
    void seek(Duration to);
    void stop();
    void skip_back();
    void skip_ahead();
    void setOnEndOfMedia(Runnable action);
    Duration getTotalDuration();
}
