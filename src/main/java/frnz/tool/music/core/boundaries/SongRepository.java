package frnz.tool.music.core.boundaries;

import frnz.tool.music.core.enities.Song;

import java.util.List;

public interface SongRepository {
    void save(Song song);

    List<Song> getAll();

    List<Song> findByTags(List<String> collect);

    List<Song> findByOriginalFileName(String name);

    boolean existsByOriginalFileName(String name);

    List<Song> findByNameContains(String query);

    List<Song> findByArtist(String artist);

    void updateRating(Integer songId, Integer newRating);
}
