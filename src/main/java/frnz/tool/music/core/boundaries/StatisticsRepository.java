package frnz.tool.music.core.boundaries;

public interface StatisticsRepository {
    void updateListenTime(Integer songId, Long secondsListened);

    void updateSkipCount(Integer songId);

    void startRecording();

    long stopRecording();

    void pauseRecording();

    void unpauseRecording();
}
