package frnz.tool.music.core.enities;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class Song {
    private int id;
    private String name;
    private String artist;
    private String originalFileName;
    private String fullPath;
    private String creationDate;
    private int rating;
    private Set<String> tags;
}
